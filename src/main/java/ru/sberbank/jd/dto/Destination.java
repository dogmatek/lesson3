package ru.sberbank.jd.dto;

public enum Destination {
    HOME (1, "���"),
    SH (2, "������� �����"),
    PROM (3, "������������ �����"),
    COM (3, "����� ��� ������������ �������������");

    private Integer number;
    private String name;

    Destination (Integer number, String name) {
        this.number = number;
        this.name = name;

    }

}
