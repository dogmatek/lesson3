package ru.sberbank.jd;

import ru.sberbank.jd.dto.Destination;
import ru.sberbank.jd.dto.Door;
import ru.sberbank.jd.dto.Land;

public class Application {
    public static void main(String[] args) {
        // 1 �������
        Land land = new Land();
        land.setAdress("�. ��������, ��. ���������");
        land.setArea(500);
        land.setDestination(Destination.HOME);


        // 2 ������� + 1
        try (Door door = new Door()){
            door.open(); // ��������� �����

            System.out.println("��������� 1");

            System.out.println("����� ��������� �������");
            land.dimensions();



            door.broken();
            System.out.println("��������� 2");

        } catch (Exception e) {
            e.printStackTrace();

            System.out.println("����������� ����������");
        }
    }

}
